import 'package:flutter/material.dart';
import 'package:flutterapp/models/Product.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TodayScreen extends StatefulWidget {
  const TodayScreen({Key? key}) : super(key: key);

  @override
  State<TodayScreen> createState() => _TodayScreenState();
}

class _TodayScreenState extends State<TodayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        title: Text('Thursday 12 May',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Color.fromRGBO(103, 103, 103, 1),
                fontFamily: 'Inter',
                fontSize: 18,
                letterSpacing: 0,
                fontWeight: FontWeight.normal,
                height: 1)),
      ),
      body: Container(
        child: ListView(
          children: [
            Container(
              width: 400,
              height: 76,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,
                    left: 20,
                    child: Text('Today',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontFamily: 'Inter',
                            fontSize: 40,
                            letterSpacing: 0,
                            fontWeight: FontWeight.bold,
                            height: 1)),
                  ),
                  Positioned(
                    top: 0,
                    left: 340,
                    child: Container(
                      width: 45,
                      height: 45,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(196, 196, 196, 1),
                        image: DecorationImage(
                            image: AssetImage('assets/images/Avatar.png'),
                            fit: BoxFit.fitWidth),
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(45, 45)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Stack(
                    children: [
                      Ink.image(
                        fit: BoxFit.cover,
                        height: 300,
                        image: AssetImage("assets/images/Applemusic.png"),
                      ),
                      Positioned(
                          top: 20,
                          left: 20,
                          child: Text("APPLE MUSIC",
                              style: TextStyle(
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  fontFamily: 'Inter',
                                  fontSize: 20,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.normal,
                                  height: 1)))
                    ],
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(196, 196, 196, 1),
                            image: DecorationImage(
                                image: AssetImage('assets/images/Tmusic.png'),
                                fit: BoxFit.fitWidth),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "Apple Music",
                              style: TextStyle(
                                color: Color.fromARGB(255, 139, 139, 139),
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "T Music",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Music",
                              style: TextStyle(
                                color: Color.fromARGB(255, 139, 139, 139),
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 90,
                        ),
                        TextButton(
                            style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(14)),
                                backgroundColor:
                                    Color.fromARGB(255, 225, 225, 225),
                                padding: EdgeInsets.only(left: 4, right: 4)),
                            onPressed: () {},
                            child: Text("GET"))
                      ],
                    ),
                  ),
                  SizedBox(height: 14)
                ],
              ),
            ),
            SizedBox(height: 20),
            Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "OUR FAVORITES",
                    style: TextStyle(
                      fontSize: 20,
                      color: Color.fromARGB(255, 173, 173, 173),
                    ),
                  ),
                  Text(
                    "What's we're using now",
                    style: TextStyle(
                      fontSize: 24,
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  ItemCard(product: products[0]),
                  ItemCard(product: products[1]),
                  ItemCard(product: products[2]),
                  ItemCard(product: products[3]),
                  ItemCard(product: products[4]),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  final Product product;
  const ItemCard({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              color: Color.fromRGBO(196, 196, 196, 1),
              image: DecorationImage(
                  image: AssetImage(product.image), fit: BoxFit.fitWidth),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                product.title,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                product.description,
                style: TextStyle(
                  color: Color.fromARGB(255, 139, 139, 139),
                  fontSize: 15,
                ),
              ),
            ],
          ),
          SizedBox(
            width: 90,
          ),
          TextButton(
              style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14)),
                  backgroundColor: Color.fromARGB(255, 225, 225, 225),
                  padding: EdgeInsets.only(left: 4, right: 4)),
              onPressed: () {},
              child: Text("GET"))
        ],
      ),
    );
  }
}
