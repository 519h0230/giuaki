import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int price, id;
  final double rating;

  Product(
      {required this.id,
      required this.image,
      required this.description,
      required this.price,
      required this.title,
      required this.rating});
}

List products = [
  Product(
    id: 1,
    title: "Grab",
    price: 0,
    image: "assets/images/Grab.png",
    description: "Đặt đồ ăn      ",
    rating: 3.9,
  ),
  Product(
    id: 2,
    title: "TikTok",
    price: 0,
    image: "assets/images/Tiktok.png",
    description: "Kết nối tài     ",
    rating: 4.8,
  ),
  Product(
    id: 3,
    title: "Capcut",
    price: 0,
    image: "assets/images/Capcut.png",
    description: "Ghép nhạc    ",
    rating: 3.6,
  ),
  Product(
    id: 4,
    title: "Youtube",
    price: 0,
    image: "assets/images/Youtube.png",
    description: "Ứng dụng YT",
    rating: 4.6,
  ),
  Product(
    id: 5,
    title: "Facebook",
    price: 0,
    image: "assets/images/Facebook.png",
    description: "Mạng xã hội ",
    rating: 3.7,
  ),
];
